#include <iostream>
#include <conio.h>

using namespace std;

enum Rank : int
{
	TWO = 2,
	THREE,
	FOUR,
	FIVE,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN,
	JACK,
	QUEEN,
	KING,
	ACE
};

enum Suit
{
	DIAMOND,
	CLUB,
	SPADE,
	HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};

//function prototypes
void Printcard(Card card);
Card Highcard(Card card1, Card card2);

int main()
{
	Card card;
	Card card1, card2;

	card.rank = ACE;
	card.suit = SPADE;

	card1.rank = TWO;
	card1.suit = HEART;

	card2.rank = TEN;
	card2.suit = DIAMOND;

	Printcard(Highcard(card, card2));

	(void)_getch();
	return 0;
}

void Printcard(Card card) // Function for printing the rank and suit of card
{
	switch (card.rank)
	{
	case ACE: cout << "The " << "Ace" << " Of "; break;
	case TWO: cout << "The " << "Two" << " Of "; break;
	case THREE: cout << "The " << "Three" << " Of "; break;
	case FOUR: cout << "The " << "Four" << " Of "; break;
	case FIVE: cout << "The " << "Five" << " Of "; break;
	case SIX: cout << "The " << "Six" << " Of "; break;
	case SEVEN: cout << "The " << "Seven" << " Of "; break;
	case EIGHT: cout << "The " << "Eight" << " Of "; break;
	case NINE: cout << "The " << "Nine" << " Of "; break;
	case TEN: cout << "The " << "Ten" << " Of "; break;
	case JACK: cout << "The " << "Jack" << " Of "; break;
	case QUEEN: cout << "The " << "Queen" << " Of "; break;
	case KING: cout << "The " << "King" << " Of "; break;
	}
	switch (card.suit)
	{
	case DIAMOND: cout << "Diamonds"; break;
	case CLUB: cout << "Clubs"; break;
	case SPADE: cout << "Spades"; break;
	case HEART: cout << "Hearts"; break;
	}
}

Card Highcard(Card card1, Card card2) // Function that will select the higher value card between 
{									  // card1 and card2, will default to card2 if they are tied in value
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	return card2;
}
